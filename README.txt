
-- SUMMARY --

Wysiwyg CKEditor Extras Provides extra CKEditor-specific options to sites
using Wysiwyg API module and the CKEditor library.  This module depends
Wysiwyg API module and the CKEditor third-party editor JavaScript library.

For a full description of the module, visit the sandbox project page:
  http://drupal.org/sandbox/amanaplan/1212480
To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/1212480


-- REQUIREMENTS --

* Wysiwyg API module
* A Wysiwyg profile using the CKEditor JavaScript library


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Setup editor profiles in Administration � Configuration � Content authoring
  � Wysiwyg.
* Configure the CKEditor-specific options in profiles using CKEditor.


-- CONTACT --

Current maintainer:
* Christian Stuck (amanaplan) - http://drupal.org/user/980604

