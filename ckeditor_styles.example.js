CKEDITOR.stylesSet.add( 'ckeditor_styles', [

  /* Block Styles */

  { name : 'Blue Title', element : 'h2', styles : { 'color' : 'Blue' } },
  { name : 'Red Title' , element : 'h3', styles : { 'color' : 'Red' } },
  { name : 'Normal Text' , element : 'p', attributes : { 'class' : '' } },

  /* Inline Styles */

  { name : 'CSS Style', element : 'span', attributes : { 'class' : 'my_style' } },
  { name : 'Marker: Yellow', element : 'span', styles : { 'background-color' : 'Yellow' } }

  /* Object Styles */

  { name : 'Image on Left', element : 'img', attributes : { 'class' : 'image-left' } },
  { name : 'Image on Right', element : 'img', attributes : { 'class' : 'image-right' } },
  { name : 'Solid Line', element : 'hr', attributes : { 'class' : '' } },
  { name : 'Dashed Line', element : 'hr', attributes : { 'class' : 'dashed' } }

]);
